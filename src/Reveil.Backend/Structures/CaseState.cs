namespace Reveil.Backend.Structures {
  public enum CaseState {
    Open,
    InProgress,
    OnHold,
    Closed,
    Archived
  }
}
