using CSharpFunctionalExtensions;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Structures;

namespace Reveil.Backend.Interfaces {
  public interface ICasesBl {
    Result<SuccessResponse, ErrorResponse> DeleteCase(string caseId);
    Result<CaseResponse, ErrorResponse> GetCase(string caseId);
    Result<SuccessResponse, ErrorResponse> CaseExists(string caseId);
    Result<ChildrenListResponse, ErrorResponse> GetAllCases();
    Result<SuccessResponse, ErrorResponse> OpenCase(string caseId, string authorizationToken);
    Result<SuccessResponse, ErrorResponse> CloseCase(string caseId);
    Result<SuccessResponse, ErrorResponse> UpdateCaseState(string caseId, CaseState state);
  }
}
