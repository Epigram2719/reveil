// ReSharper disable PropertyCanBeMadeInitOnly.Global
namespace Reveil.Backend.Models.Configuration {
  public class PersistenceConfiguration {
    public string DataRoot { get; set; } = "./";
  }
}
