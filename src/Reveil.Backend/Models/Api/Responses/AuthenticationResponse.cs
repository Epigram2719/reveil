// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using System;
using Reveil.Backend.Models.Api.Responses.Core;

namespace Reveil.Backend.Models.Api.Responses {
  public class AuthenticationResponse : SuccessResponse {
    public string UserId { get; set; }
    public string AuthorizationToken { get; set; }
    public TimeSpan ValidityDuration { get; set; }
  }
}
