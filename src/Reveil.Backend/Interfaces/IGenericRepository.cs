using System;
using System.Collections.Generic;
using CSharpFunctionalExtensions;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Interfaces {
  public interface IGenericRepository<T> where T : BaseEntity {
    Result<T> Add(T entity);
    Result<Maybe<T>> Get(string id);
    Result<Maybe<T>> Get(Predicate<T> predicate);
    Result<IList<T>> GetAll(Predicate<T> predicate);
    Result Remove(Predicate<T> predicate);
    Result Remove(string id);
  }
}
