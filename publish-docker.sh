#!/usr/bin/env sh

docker build -t "$CI_REGISTRY/$CI_PROJECT_PATH/reveil:$CI_COMMIT_TAG" -t "$CI_REGISTRY/$CI_PROJECT_PATH/reveil:latest" .
docker push "$CI_REGISTRY/$CI_PROJECT_PATH/reveil:latest"
docker push "$CI_REGISTRY/$CI_PROJECT_PATH/reveil:$CI_COMMIT_TAG"
