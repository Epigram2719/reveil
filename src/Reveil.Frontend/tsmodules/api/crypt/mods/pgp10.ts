import { ApiCryptModsIMod } from './imod'

import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'


export class ApiCryptModsPGP10<T> implements ApiCryptModsIMod<T> {
    constructor(inject: ApiEndpointExtensionLayer) {

    }

    Encrypt(data: T): string {
        
        throw Error("not implemented! This class is used as interface.")
    }

    Decrypt(data: string): T {
        throw Error("not implemented! This class is used as interface.")
    }

    /*
    async GetServerKey(): Promise<openpgp.Key> {
        // TODO: validate data here.
        return openpgp.readKey({ armoredKey: this.bootstrap.apiPublicKey || '' });
    }

    async GetClientKey(user_id: openpgp.UserID, passphrase: string): Promise<openpgp.KeyPair> {
        // TODO: validate data here.
        return openpgp.generateKey({
            type: 'ecc',
            curve: 'curve25519',
            userIDs: [ user_id ],
            passphrase: passphrase,
            format: 'object',
        })
    }

    async EncryptObject<T>(obj: T, public_key: openpgp.Key): Promise<string> {
        return openpgp.encrypt({
            message: await openpgp.createMessage({ text: JSON.stringify(obj) }),
            encryptionKeys: public_key,

            // optional
            signingKeys: await openpgp.decryptKey({
                privateKey: await openpgp.readPrivateKey({ armoredKey: keys.private_key }),
                passphrase: passphrase
            }),
        });
    }

    async DecryptObject<T>(str: string, private_key: openpgp.PrivateKey, passphrase: string): Promise<T> {
        return openpgp.decrypt({
            message: await openpgp.readMessage({
                armoredMessage: str,
            }),
            verificationKeys: private_key,
            decryptionKeys: await openpgp.decryptKey({
                privateKey: private_key,
                passphrase: passphrase,
            }),
        })
        .then((e) => {
            return JSON.parse(e.data) as T
        });
    }
    */
}
