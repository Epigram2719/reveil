namespace Reveil.Backend.Interfaces {
  public interface INotificationSenderFactory {
    INotificationSender Create();
  }
}
