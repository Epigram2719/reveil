namespace Reveil.Backend.Tests.PersistenceProvider {
  public enum RepoType {
    FilePersisted,
    InMemory
  }
}
