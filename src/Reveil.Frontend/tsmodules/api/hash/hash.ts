import { ApiHash01 } from './mods/hash01'

import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'


export enum ApiHashType {
    PBKDF2_Y_I999_L64_SHA256 = 1,
}

export class ApiHash {
    // hash :: facade
    static Hash(hash: ApiHashType, data: string, inject: ApiEndpointExtensionLayer): Promise<string> {
        if(hash === 1) {
            return ApiHash01.Hash(data, inject)
        }

        throw Error("Error. Module not found.")
    }

    static ApiHashTypeByIndex(index: Number): ApiHashType {
        if(index === 1) {
            return ApiHashType.PBKDF2_Y_I999_L64_SHA256
        }
        throw Error("Unable to find ApiCryptType with index " + index)
    }
}
