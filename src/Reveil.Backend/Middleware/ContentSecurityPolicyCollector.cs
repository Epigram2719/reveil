// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Reveil.Backend.Middleware {
  /// <summary>
  /// A simple CSP Generator that builds a CSP that "just works".
  /// Benchmarked with: https://csp-evaluator.withgoogle.com/
  /// Additionally verified tested against Firefox and Chromium
  /// </summary>
  public class ContentSecurityPolicyCollector {
    public List<string> JsHashes { get; set; }
    public List<string> CssHashes { get; set; }

    public List<string> FontHashes { get; set; }
    public List<string> ImageHashes { get; set; }

    public ContentSecurityPolicyCollector(string directory) {
      var imageFiles = Directory.GetFiles(directory, "*.png", SearchOption.AllDirectories)
        .Concat(Directory.GetFiles(directory, "*.ico", SearchOption.AllDirectories))
        .Concat(Directory.GetFiles(directory, "*.jpg", SearchOption.AllDirectories))
        .Concat(Directory.GetFiles(directory, "*.jpeg", SearchOption.AllDirectories))
        .Concat(Directory.GetFiles(directory, "*.svg", SearchOption.AllDirectories));

      var fontFiles = Directory.GetFiles(directory, "*.woff", SearchOption.AllDirectories)
        .Concat(Directory.GetFiles(directory, "*.ttf", SearchOption.AllDirectories))
        .Concat(Directory.GetFiles(directory, "*.eot", SearchOption.AllDirectories))
        .Concat(Directory.GetFiles(directory, "*.woff2", SearchOption.AllDirectories));

      JsHashes = Directory
        .GetFiles(directory, "*.js", SearchOption.AllDirectories)
        .Select(Hash)
        .Where(h => h != null)
        .Select(h => h!)
        .ToList();

      CssHashes = Directory
        .GetFiles(directory, "*.css", SearchOption.AllDirectories)
        .Select(Hash)
        .Where(h => h != null)
        .Select(h => h!)
        .ToList();

      FontHashes = fontFiles
        .Select(Hash)
        .Where(h => h != null)
        .Select(h => h!)
        .ToList();
      
      ImageHashes = imageFiles
        .Select(Hash)
        .Where(h => h != null)
        .Select(h => h!)
        .ToList();
    }

    public string Build() {
      var sb = new StringBuilder();

      sb.Append("base-uri 'self'; ");
      sb.Append("media-src 'none'; ");
      sb.Append("object-src 'none'; ");
      sb.Append("frame-src 'none'; ");
      sb.Append("child-src 'none'; ");
      sb.Append("default-src 'self'; ");
      sb.Append("worker-src 'self'; ");
      sb.Append("form-action 'none'; ");
      sb.Append("connect-src 'self'; ");
      sb.Append("prefetch-src 'self'; ");
      sb.Append("manifest-src 'self'; ");
      sb.Append("frame-ancestors 'none'; ");
      // sb.Append("upgrade-insecure-requests; ");
      sb.Append("sandbox allow-downloads allow-modals allow-scripts allow-same-origin; ");

      sb.Append($"img-src 'self' {string.Join(" ", ImageHashes.Select(h => $"'{h}'"))}; ");
      sb.Append($"font-src 'self' {string.Join(" ", FontHashes.Select(h => $"'{h}'"))}; ");
      sb.Append($"script-src 'self' {string.Join(" ", JsHashes.Select(h => $"'{h}'"))}; ");
      sb.Append($"script-src-elem 'self' {string.Join(" ", JsHashes.Select(h => $"'{h}'"))}; ");
      sb.Append($"style-src 'self' {string.Join(" ", CssHashes.Select(h => $"'{h}'"))}; ");
      sb.Append($"style-src-elem 'self' {string.Join(" ", CssHashes.Select(h => $"'{h}'"))}; ");
      return sb.ToString();
    }

    private string Hash(string filePath) {
      try {
        using var sha256 = new SHA256Managed();
        var fileContent = File.ReadAllBytes(filePath);
        var hashed = sha256.ComputeHash(fileContent);
        return $"sha256-{Convert.ToBase64String(hashed, Base64FormattingOptions.None)}";
      } catch { return null; }
    }
  }
}
