import { ApiCacheModsIMod } from './mods/imod'
import { ApiCacheModsNone } from './mods/none'
import { ApiCacheModsNuxt } from './mods/nuxt'
import { ApiCacheModsStorage } from './mods/storage'


import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'

export enum ApiCacheType {
    None = 1,
    Nuxt,
    LocalStorage,
}

export class ApiCache {
    static Proxy<T>(cache_type: ApiCacheType, name: string, inject: ApiEndpointExtensionLayer): ApiCacheModsIMod<T> {
        if(cache_type === ApiCacheType.None) {
            // desc: none cache
            return new ApiCacheModsNone<T>(name, inject);
        }
        if(cache_type === ApiCacheType.Nuxt) {
            // desc: nuxt-store cache
            return new ApiCacheModsNuxt<T>(name, inject);
        }
        if(cache_type === ApiCacheType.LocalStorage) {
            // desc: storage cache
            return new ApiCacheModsStorage<T>(name, inject);
        }

        throw Error("Error. Module not found.")
    }
}
