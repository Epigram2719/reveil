import { ApiFileMetaModsJPEG } from './mods/jpeg'

import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'


export class ApiFileMeta {
    // wipemeta :: facade
    static WipeMeta(file: File, content: string, inject: ApiEndpointExtensionLayer): string {
        if(file.name.endsWith('.jpg') || file.name.endsWith('.jpeg')) {
            return ApiFileMetaModsJPEG.WipeMeta(file, content, inject)
        }

        return content
    }
}
