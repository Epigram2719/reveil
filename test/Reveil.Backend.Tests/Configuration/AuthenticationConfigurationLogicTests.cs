using FluentAssertions;
using Reveil.Backend.Models.Configuration.Authentication;
using Xunit;

namespace Reveil.Backend.Tests.Configuration {
  public class AuthenticationConfigurationLogicTests {
    [Theory]
    [InlineData("LDAP")]
    [InlineData("Ldap")]
    [InlineData("ldap")]
    [InlineData("STATIC")]
    [InlineData("Static")]
    [InlineData("static")]
    public void TestCanUserGivenTypeMapToConcreteConfigurationType(string userGivenType) {
      var availableConfigurations = AuthenticationConfigurationLogic.GetAvailableAuthenticationProviderConfigurationTypes();
      availableConfigurations.GetAuthenticationProviderConfigurationTypeByName(userGivenType).Should().NotBeNull();
    }

    [Theory]
    [InlineData("")]
    [InlineData(" ")]
    [InlineData(null)]
    [InlineData("AAAA-AAAA-AAAA-AAAA-AAAA-AAAA-AAAA")]
    [InlineData("Not:Existing_Auth-Provider*With?Special!Character;In$The%Name&/~$%&/()=?!c")]
    public void TestNotKnownUserGivenTypeDoesNotReturnAValidConfigurationType(string userGivenType) {
      var availableConfigurations = AuthenticationConfigurationLogic.GetAvailableAuthenticationProviderConfigurationTypes();
      availableConfigurations.GetAuthenticationProviderConfigurationTypeByName(userGivenType).Should().BeNull();
    }
  }
}
