import webpack from 'webpack'
import * as cheerio from 'cheerio';

export default {
    // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
    ssr: false,

    // Target: https://go.nuxtjs.dev/config-target
    target: 'static',

    // Loading: https://nuxtjs.org/docs/features/loading/#disable-the-progress-bar
    // loading: false,

    // loading-indicator: https://nuxtjs.org/docs/features/loading/#the-loading-indicator-property
    loadingIndicator: false,

    // workaround for inline-script: https://github.com/nuxt/nuxt.js/issues/2822
    hooks: {
        'generate:page': page => {
            const doc = cheerio.load(page.html);
            doc('body script').each((index, item) => {
                try {
                    if(item.children[0].data.startsWith('window.__NUXT__')) {
                        doc(item).remove();
                    }
                }
                catch(e) {
                    // the silent are the worst.
                }
                page.html = doc.html();
            })
        },
    },

    render: {
        resourceHints: false
    },

    // Server: https://nuxtjs.org/docs/configuration-glossary/configuration-server/
    server: {
        host: "0.0.0.0",
        port: 5000,
    },

    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        titleTemplate: '%s - Reveil',
        title: 'Reveil.Frontend',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' },
            { name: 'format-detection', content: 'telephone=no' },
            // { name: 'Content-Security-Policy', content: "script-src 'strict-dynamic'" + x['default'].join(' ') }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
        ]
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
        'assets/css/fonts.css',
        'assets/css/theme.css',
        'assets/css/transition.css',
        'quill/dist/quill.snow.css',
        '@mdi/font/css/materialdesignicons.css'
    ],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
        { src: '@/plugins/tsmapi' },
        { src: '@/plugins/transl8' },
        { src: '@/plugins/locales' },
    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [
        // https://go.nuxtjs.dev/typescript
        '@nuxt/typescript-build',
        // https://go.nuxtjs.dev/vuetify
        '@nuxtjs/vuetify',
    ],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        // https://i18n.nuxtjs.org
        [
            '@nuxtjs/i18n', {
                locales: [
                    { code: 'bg', file: 'bg.json' },
                    { code: 'hr', file: 'hr.json' },
                    { code: 'cs', file: 'cs.json' },
                    { code: 'da', file: 'da.json' },
                    { code: 'nl', file: 'nl.json' },
                    { code: 'en', file: 'en.json' },
                    { code: 'et', file: 'et.json' },
                    { code: 'fi', file: 'fi.json' },
                    { code: 'fr', file: 'fr.json' },
                    { code: 'de', file: 'de.json' },
                    { code: 'el', file: 'el.json' },
                    { code: 'hu', file: 'hu.json' },
                    { code: 'ga', file: 'ga.json' },
                    { code: 'it', file: 'it.json' },
                    { code: 'lv', file: 'lv.json' },
                    { code: 'lt', file: 'lt.json' },
                    { code: 'mt', file: 'mt.json' },
                    { code: 'pl', file: 'pl.json' },
                    { code: 'pt', file: 'pt.json' },
                    { code: 'ro', file: 'ro.json' },
                    { code: 'sk', file: 'sk.json' },
                    { code: 'sl', file: 'sl.json' },
                    { code: 'es', file: 'es.json' },
                    { code: 'sv', file: 'sv.json' },
                ],
                lazy: true,
                langDir: 'lang/',
                strategy: 'no_prefix',
            }
        ],
    ],

    i18n: {
        detectBrowserLanguage: {
            useCookie: true,
        }
    },

    // Axios module configuration: https://go.nuxtjs.dev/config-axios
    axios: {
        // baseUrl: process.env.BACKEND_API
        proxy: true,
    },

    // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
    vuetify: {
        defaultAssets: {
            font: false,
            icons: false,
        },
        
        // treeShake: true,
        
        icons: {
            iconfont: 'mdi',
        },

        font: {
            family: 'Baloo2-Regular',
        },

        customVariables: ['~/assets/variables.scss'],

        theme: { disable: true },
    },

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {
        // webpack :: https://webpack.js.org/configuration/
        plugins: [
            new webpack.ProvidePlugin({
                global: require.resolve('./global.js')
            })
        ],
        extend(config, { isClient }) {
            config.target = 'web';
            config.devtool = false;
            config.node = {
                // prevent webpack from injecting useless setImmediate polyfill because Vue
                // source contains it (although only uses it if it's native).
                setImmediate: false,
                // prevent webpack from injecting eval / new Function through global polyfill
                global: false
            }
            config.mode = 'production';
        },

        // https://nuxtjs.org/docs/configuration-glossary/configuration-build#extractcss
        extractCSS: true,
        optimization: {
            splitChunks: {
                cacheGroups: {
                    styles: {
                        name: 'styles',
                        test: /\.(css|vue)$/,
                        chunks: 'all',
                        enforce: true
                    }
                }
            }
        },

        // https://nuxtjs.org/docs/configuration-glossary/configuration-build#friendlyerrors
        friendlyErrors: true,

        // https://nuxtjs.org/docs/configuration-glossary/configuration-build#analyze
        // analyze: {
        //     analyzerMode: 'static'
        // },

        // https://nuxtjs.org/docs/configuration-glossary/configuration-build#htmlminify
        html: { 
            minify: {
                collapseBooleanAttributes: true,
                decodeEntities: true,
                minifyCSS: true,
                minifyJS: true,
                processConditionalComments: true,
                removeEmptyAttributes: true,
                removeRedundantAttributes: true,
                trimCustomFragments: true,
                useShortDoctype: true
            }
        },

        // https://nuxtjs.org/docs/configuration-glossary/configuration-build/#optimization
        optimization: {
            minimize: true,
        },
    },

    ignore: [
        '**/*.test.ts',
        '**/*.test.js',
    ],

    // Router: https://nuxtjs.org/docs/configuration-glossary/configuration-router/
    router: {
        middleware: [
            'rigproof',
            'sitelogic',
        ]
    },
}
