import * as api from '~/api'

import { IModelMgmt } from './models'

import { ApiEndpointExtensionLayer, ApiEndpointExtensionExtend } from '~/tsmodules/api/_eel'
import { ApiCache, ApiCacheModsIMod, ApiCacheType} from '~/tsmodules/api/cache'
import { ApiCrypt, ApiCryptModsIMod, ApiCryptType } from '~/tsmodules/api/crypt'
import { ApiBootstrap, ApiBootstrapModsIMod, ApiBootstrapType } from '~/tsmodules/api/bootstrap'

export class ApiMgmt extends ApiEndpointExtensionExtend {
    constructor(eel: ApiEndpointExtensionLayer) {
        super(eel)
    }
    
    // >!< >!< >!< >!< >!< >!< >!< >!< >!< >!< 
    // >!< mgmt
    // >!< >!< >!< >!< >!< >!< >!< >!< >!< >!< 
    async MgmtGet(mgmt_id: string): Promise<IModelMgmt | null> {
        // validate
        if(mgmt_id === '') {
            return null;
        }

        // cache
        var cache = ApiCache.Proxy<Array<IModelMgmt>>(this.eel!.CacheType, "mgmt", this.eel!)
        var cache_find = cache.GetCache()?.find((e) => e.response.id === mgmt_id) || null
        if(cache_find !== null) {
            return cache_find
        }

        // api
        return new api.UsersApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).getUser(mgmt_id)
        .then((e) => {
            if(e.data.isSuccess === true) {
                var rtvl = {
                    response: e.data
                } as IModelMgmt
    
                cache.SetCache((cache.GetCache() || [rtvl]))
    
                return rtvl
            }
            return null;
        })
        .catch((e) => {
            return null
        })
    }
}