using System.Net.Http.Json;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Flurl.Http;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Configuration.Notification;

namespace Reveil.Backend.Notifications.Webhooks.Discord {
  public class DiscordNotifier : INotificationSender {
    private readonly DiscordNotificationTargetConfiguration _webhookNotificationTargetConfiguration;
    public string Notifier => nameof(DiscordNotifier);

    public DiscordNotifier(DiscordNotificationTargetConfiguration webhookNotificationTargetConfiguration) {
      _webhookNotificationTargetConfiguration = webhookNotificationTargetConfiguration;
    }

    public async Task<Result<Notification, NotificationError>> SendAsync(Notification notification) {
      try {
        var message = new DiscordMessageMapper().Map(notification, _webhookNotificationTargetConfiguration);
        await _webhookNotificationTargetConfiguration.WebhookUrl.PostAsync(JsonContent.Create(message));
        return Result.Success<Notification, NotificationError>(notification);
      } catch(FlurlHttpException) {
        return Result.Failure<Notification, NotificationError>(new NotificationError());
      }
    }
  }
}
