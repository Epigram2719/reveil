using System.Net;
using Microsoft.AspNetCore.Mvc;
using Reveil.Backend.Extensions;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Middleware.Filter;
using Reveil.Backend.Models.Api.Requests;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;

namespace Reveil.Backend.Controllers {
  [ApiController]
  [Route("[controller]")]
  [Produces("application/json")]
  [Consumes("application/json")]
  public class ContentController : ControllerBase {
    private readonly IContentBl _contentBl;

    public ContentController(IContentBl contentBl) {
      _contentBl = contentBl;
    }

    /// <summary>
    /// Gets a content by its type and its language
    /// </summary>
    /// <returns></returns>
    [HttpGet("/content/{contentType}/{languageCode}", Name = nameof(GetContent))]
    [ProducesResponseType(typeof(ContentEntryResponse), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse GetContent(string contentType, string languageCode) =>
      _contentBl.GetContent(contentType, languageCode).MatchAndSetStatusCode(HttpContext);

    /// <summary>
    /// Sets and/or overwrites existing ("add or update") content by its type and its language
    /// </summary>
    /// <returns></returns>
    [HttpPost("/content", Name = nameof(AddOrUpdateContent))]
    [RequireAuthorization]
    [ProducesResponseType(typeof(ContentEntryResponse), (int)HttpStatusCode.Created)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse AddOrUpdateContent([FromBody] AddOrUpdateContentRequest request) =>
      _contentBl.AddOrUpdateContent(request.ContentType, request.LanguageCode, request.Content).MatchAndSetStatusCode(HttpContext);
  }
}
