using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc.Testing;
using Reveil.Backend.Models.Api.Requests;
using Xunit;

namespace Reveil.Backend.IntegrationTests {
  public class UnauthenticatedRequestTests : IClassFixture<ReveilTestWebApplicationFactory<Startup>> {
    private readonly HttpClient _httpClient;

    public UnauthenticatedRequestTests(ReveilTestWebApplicationFactory<Startup> factory) {
      _httpClient = factory.CreateClient(new WebApplicationFactoryClientOptions {
        AllowAutoRedirect = false
      });
    }

    [Theory]
    [InlineData("/cases")]
    [InlineData("/cases/123")]
    [InlineData("/cases/123/journal")]
    [InlineData("/cases/123/journal/123")]
    [InlineData("/cases/123/messages")]
    [InlineData("/cases/123/messages/123")]
    [InlineData("/cases/123/messages/123/files")]
    [InlineData("/cases/123/messages/123/files/123/meta")]
    [InlineData("/cases/123/messages/123/files/123/data")]
    public async Task TestHttpGetProducesUnauthorizedStatusCode(string endpoint) {
      var result = await _httpClient.GetAsync(endpoint);
      result.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
    }

    [Fact]
    public async Task TestHttpPostToContentEndpointProducesUnauthorizedStatusCode() {
      var body = new AddOrUpdateContentRequest {Content = "Hello", LanguageCode = "DE", ContentType = "Text"};
      var result = await _httpClient.PostAsync("/content", JsonContent.Create(body));
      result.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
    }

    [Theory]
    [InlineData("/version/backend")]
    [InlineData("/bootstrap/config.json")]
    public async Task TestHttpGetOfPubliclyAvailableEndpointsAreStatusCode200(string endpoint) {
      var result = await _httpClient.GetAsync(endpoint);
      result.StatusCode.Should().Be(HttpStatusCode.OK);
    }
  }
}
