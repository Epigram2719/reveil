// ReSharper disable PropertyCanBeMadeInitOnly.Global

namespace Reveil.Backend.Models.Api.Requests {
  public class AddOrUpdateContentRequest {
    public string ContentType { get; set; } 
    public string LanguageCode { get; set; }
    public string Content { get; set; }
  }
}
