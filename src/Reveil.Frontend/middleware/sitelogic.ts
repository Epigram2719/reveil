import { Middleware } from '@nuxt/types'

const sitelogic: Middleware = async (context) => {
    var locales_get = function(): boolean {
        return (
            !!sessionStorage.getItem('locales_language') === true &&
            !!sessionStorage.getItem('locales_country') === true &&
            !!Number(sessionStorage.getItem('locales_privacypolicy')) === true
        )
    }

    var mgmt_get = function(): boolean {
        return context.$tsmapi.authmgmt().AuthMgmtState() !== null
    }

    var user_get = function(): boolean {
        return context.$tsmapi.authuser().AuthUserState() !== null
    }

    // sitelogic
    // require_*_redirect only with trailing slash "/"
    var sl = [
        { 
            name: 'error',
            prefix: '/e',
            require_user: false, 
            require_user_redirect: '', 
            require_mgmt: false, 
            require_mgmt_redirect: '',
            require_locales: false,
            require_locales_redirect: '',
        },
        { 
            name: 'jump',
            prefix: '/j',
            require_user: false, 
            require_user_redirect: '', 
            require_mgmt: false, 
            require_mgmt_redirect: '',
            require_locales: false,
            require_locales_redirect: '',
        },
        { 
            name: 'public',
            prefix: '/p',
            require_user: false, 
            require_user_redirect: '', 
            require_mgmt: false, 
            require_mgmt_redirect: '',
            require_locales: true,
            require_locales_redirect: '/p/intro',
        },
        { 
            name: 'mgmt',
            prefix: '/m',
            require_user: false, 
            require_user_redirect: '', 
            require_mgmt: true, 
            require_mgmt_redirect: '/m/login/',
            require_locales: true,
            require_locales_redirect: '/p/intro',
        },
        { 
            name: 'user',
            prefix: '/u',
            require_user: true, 
            require_user_redirect: '/u/login/', 
            require_mgmt: false, 
            require_mgmt_redirect: '',
            require_locales: true,
            require_locales_redirect: '/p/intro',
        },
    ];

    var policy = sl.find((e) => context.route.path.startsWith(e.prefix)) || null;
    if(policy === null) {
        return
    }

    // locales
    if( 
        policy.require_locales === true && 
        locales_get() === false && 
        policy.require_locales_redirect !== '' &&
        context.route.path !== policy.require_locales_redirect
    ) {
        context.redirect(policy.require_locales_redirect, { ...{ r: context.route.path }, ... context.query});
    }

    // mgmt
    if(
        policy.require_mgmt === true &&
        mgmt_get() === false &&
        policy.require_mgmt_redirect !== '' &&
        context.route.path !== policy.require_mgmt_redirect
    ) {
        context.redirect(policy.require_mgmt_redirect, { ...{ r: context.route.path }, ... context.query});
    }

    // user
    if(
        policy.require_user === true &&
        user_get() === false &&
        policy.require_user_redirect !== '' &&
        context.route.path !== policy.require_user_redirect
    ) {
        context.redirect(policy.require_user_redirect, { ...{ r: context.route.path }, ... context.query});
    }
}

export default sitelogic
