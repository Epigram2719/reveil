import * as api from '~/api'

import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'


export class ApiBootstrapModsIMod {
    constructor(inject: ApiEndpointExtensionLayer)
    {

    }

    async GetValues(): Promise<api.BootstrapConfigurationResponse> {
        throw Error("not implemented!");
    }

    async GetApiCfg(): Promise<api.Configuration> {
        throw Error("not implemented!");
    }
}
