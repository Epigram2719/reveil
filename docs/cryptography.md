# Cryptography

Reveil utilizes asymmetric key cryptography, namely PGP (Pretty Good Privacy). All content is End-to-End encrypted. Data-In-Transit, and Data-At-Rest is always encrypted.

## use cases

### Send Messages as Whistleblower

![Sequence Diagram for creating messages from the perspective of the Whistleblower](assets/reveil_crypto_whistleblower_send_message.drawio.svg)

### Send Responses to Whistleblowers

TBD

## Libraries

- [OpenPGP JS](https://github.com/openpgpjs/openpgpjs/)
