import pbkdf2 from 'pbkdf2'

export class Pbkdf2 {
    private salt: string;
    private iter: number;
    private klen: number;
    private dgst: string;

    constructor(salt: string, iter: number, klen: number, dgst: string = "sha512") {
        this.salt = salt
        this.iter = iter
        this.klen = klen
        this.dgst = dgst
    }

    Generate(password: string): string {
        return pbkdf2.pbkdf2Sync(
            password, 
            this.salt,
            this.iter,
            this.klen,
            this.dgst
        ).toString('hex')
    }
}
