namespace Reveil.Backend.Notifications {
  public enum NotificationType {
    ServerError,
    NewCase,
    NewMessage
  }
}
