import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'

export class ApiCacheModsIMod<T> {
    constructor(name: string, inject: ApiEndpointExtensionLayer) {

    }

    GetCache(): T | null {
        throw Error("not implemented! This class is used as interface.")
    }

    SetCache(data: T): void {
        throw Error("not implemented! This class is used as interface.")
    }

    ClearCache(): void {
        throw Error("not implemented! This class is used as interface.")
    }

    WipeCache(): void {
        throw Error("not implemented! This class is used as interface.")
    }
}
