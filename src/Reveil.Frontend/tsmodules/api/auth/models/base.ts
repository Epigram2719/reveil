export interface IModelAuthBase {
    username: string,
    password: string,
    remember: boolean,

    authtype: string,       // string => 'user' || 'mgmt'
}
