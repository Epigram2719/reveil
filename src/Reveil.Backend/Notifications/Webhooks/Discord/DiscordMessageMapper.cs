using System.Collections.Generic;
using System.Linq;
using Reveil.Backend.Models.Configuration.Notification;

namespace Reveil.Backend.Notifications.Webhooks.Discord {
  public class DiscordMessageMapper {
    public DiscordNotification Map(Notification notification, DiscordNotificationTargetConfiguration notificationTargetConfiguration) {
      var allowMentionsPolicy = new DiscordAllowedMentions();
      if (notificationTargetConfiguration.AllowMentionRoles) allowMentionsPolicy.Parse.Add("roles");
      if (notificationTargetConfiguration.AllowMentionEveryone || notificationTargetConfiguration.AllowMentionEveryoneOnline) allowMentionsPolicy.Parse.Add("everyone");
      if (notificationTargetConfiguration.CustomMentions.Any()) allowMentionsPolicy.Users.AddRange(notificationTargetConfiguration.CustomMentions);

      var mentioned = new List<string>();
      if (notificationTargetConfiguration.MentionEveryone) mentioned.Add("everyone");
      if (notificationTargetConfiguration.MentionEveryoneOnline) mentioned.Add("here");
      if (notificationTargetConfiguration.CustomMentions.Any()) mentioned.AddRange(notificationTargetConfiguration.CustomMentions);

      var content = $"{notificationTargetConfiguration.CustomMessage}{string.Join(", ", mentioned.Select(m => $"@{m}"))}";

      return new DiscordNotification {
        Content = content,
        Username = "Reveil Notifier",
        AvatarUrl = notificationTargetConfiguration.AvatarUrl,
        Mentions = allowMentionsPolicy,
        Components = null,
        Embeds = new object[] {
          new DiscordLinkEmbed {
            Title = notification.Message,
            Color = 1127128,
            Url = "https://live.rtrace.io",
            Author = new DiscordEmbedAuthor {
              Name = $"Case: {notification.CaseId}",
              Url = "https://blog.rtrace.io",
              IconUrl = "https://blog.rtrace.io/images/unknown_paradigm.jpg"
            },
            Footer = new DiscordEmbedFooter {
              Text = "© Reveil"
            }
          }
        }
      };
    }
  }
}
