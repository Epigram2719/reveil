export class ApiCryptModsIMod<T> {
    constructor() {

    }

    Encrypt(data: T): string {
        throw Error("not implemented! This class is used as interface.")
    }

    Decrypt(data: string): T {
        throw Error("not implemented! This class is used as interface.")
    }
}
