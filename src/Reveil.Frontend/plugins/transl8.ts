import { Plugin, Context } from '@nuxt/types'

// >>> typing
interface ITransl8 {
    t(data: string, mode?: string, mode_i18n_prefix?: string): string
}

// >>> data
export var data = function(context: Context): ITransl8 {
    return {
        t(data: string, mode: string = "auto", mode_i18n_prefix: string = "#::"): string {
            // mode :: i18n
            if(mode === 'i18n') {
                context.i18n.t(data.replace(mode_i18n_prefix.toLocaleLowerCase(), '')).toString()
            }

            // mode :: text
            if(mode === 'text') {
                return data.replace(mode_i18n_prefix, '')
            }

            // mode :: auto
            if(mode === 'auto' && data.startsWith(mode_i18n_prefix)) {
                return context.i18n.t(data.replace(mode_i18n_prefix, '').toLocaleLowerCase()).toString()
            } 
            return data.replace(mode_i18n_prefix, '')
        }
    }
}

declare module 'vue/types/vue' {
    interface Vue {
        $transl8: ITransl8
    }
}

declare module '@nuxt/types' {
    interface NuxtAppOptions {
        $transl8: ITransl8
    }
    
    interface Context {
        $transl8: ITransl8
    }
}

const transl8: Plugin = (context, inject) => {
    inject('transl8', data(context))
}

export default transl8
