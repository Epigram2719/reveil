using System;
using System.IO;
using Microsoft.Extensions.Options;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Configuration;
using Reveil.Backend.Models.Entities;
using Reveil.Backend.Repositories;

namespace Reveil.Backend.Tests.PersistenceProvider {
  public static class RepositoryFactory {
    public static IGenericRepository<T> Create<T>(RepoType type) where T : BaseEntity {
      var persistenceConfiguration = new PersistenceConfiguration {
        DataRoot = Path.Join(Environment.CurrentDirectory, "TestData", "Persistence", "DataRoot", Guid.NewGuid().ToString())
      };
      Directory.CreateDirectory(persistenceConfiguration.DataRoot);
      IOptions<PersistenceConfiguration> options = new OptionsWrapper<PersistenceConfiguration>(persistenceConfiguration);

      return type switch {
        RepoType.InMemory => new GenericInMemoryRepository<T>(),
        RepoType.FilePersisted => typeof(T).Name switch {
          nameof(CaseEntity) => new GenericFileSystemRepository<T>(options),
          nameof(UserEntity) => new GenericFileSystemRepository<T>(options),
          nameof(FileEntity) => (IGenericRepository<T>)new FileEntityFileSystemRepository(options),
          nameof(MessageEntity) => (IGenericRepository<T>)new MessageEntityFileSystemRepository(options),
          nameof(JournalEntity) => (IGenericRepository<T>)new JournalEntityFileSystemRepository(options),
          nameof(ContentEntity) => (IGenericRepository<T>)new ContentEntityFileSystemPersistedRepository(options),
          nameof(AuthorizationEntity) => (IGenericRepository<T>)new GenericFileSystemRepository<AuthorizationEntity>(options),
          _ => throw new NotImplementedException("Implementation for Entity not Found!")
        },
        _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
      };
    }
  }
}
