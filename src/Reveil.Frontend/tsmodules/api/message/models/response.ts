import * as api from '~/api'

export interface IModelMessageResponse {
    response: api.SuccessResponse | api.ErrorResponse
    ref_obj: string,
}
