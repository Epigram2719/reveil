using System;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Primitives;
using Reveil.Backend.Models.Configuration;

namespace Reveil.Backend.Controllers {
  [ApiController]
  [Route("[controller]")]
  [Produces("application/json")]
  [Consumes("application/json")]
  public class FrontendController : ControllerBase {
    private readonly IOptions<ReveilConfiguration> _configuration;
    private readonly IWebHostEnvironment _webHostEnvironment;

    public FrontendController(IOptions<ReveilConfiguration> configuration, IWebHostEnvironment webHostEnvironment) {
      _configuration = configuration;
      _webHostEnvironment = webHostEnvironment;
    }

    /// <summary>
    /// Get Version of Reveil.Backend component
    /// </summary>
    /// <returns></returns>
    [HttpGet("/version/backend", Name = nameof(GetBackendVersion))]
    public BackendVersionResponse GetBackendVersion() {
      return new BackendVersionResponse {
        IsDevelopmentBuild = _webHostEnvironment.IsDevelopment(),
        SourceCodeCommitSha = "TBD",
        Version = GetType().Assembly.GetName().Version?.ToString() ?? "Unknown"
      };
    }

    /// <summary>
    /// Gets the bootstrap configuration loaded by the Frontend
    /// </summary>
    /// <returns></returns>
    [HttpGet("/bootstrap/config.json", Name = nameof(GetBootstrapConfiguration))]
    public BootstrapConfigurationResponse GetBootstrapConfiguration() {
      return new BootstrapConfigurationResponse {
        Api = _configuration.Value.FullyQualifiedDomainName,
        ApiPublicKey = Guid.NewGuid().ToString(),
        I18N = new FrontendI18NConfiguration {
          DefaultCountryCode = _configuration.Value.I18N.DefaultCountryCode,
          DefaultLanguageCode = _configuration.Value.I18N.DefaultLanguageCode,
          TimeZoneInfo = _configuration.Value.I18N.Timezone
        },
        FeatureToggles = new FrontendFeatureToggles {
          EnableDebugMode = _webHostEnvironment.IsDevelopment(),
          InlineFileUploadsInMessage = true
        },
        FileUploads = new FrontendFileUploadConfiguration {
          MaxFileSizeInBytes = _configuration.Value.FileUploads.MaxFileUploadSizeInBytes,
          SupportedMediaTypes = _configuration.Value.FileUploads.AllowedFileExtensions
            .Select(e => e.ToLowerInvariant().TrimStart('.'))
            .Select(e => Constants.MediaTypes.FirstOrDefault(m => m.Extension == e))
            .Where(m => m?.Extension != null)
            .Distinct()
            .ToList()
        }
      };
    }
  }
}
