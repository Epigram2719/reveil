using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace Reveil.Backend.Controllers {
  public class ReveilBaseController : ControllerBase {
    protected string GetAuthorizationToken() => HttpContext.Request.Headers["Bearer"].FirstOrDefault();
  }
}
