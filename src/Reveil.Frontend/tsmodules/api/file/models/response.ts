import * as api from '~/api'

export interface IModelFileResponse {
    response: api.SuccessResponse | api.ErrorResponse
    ref_obj: string,
}
