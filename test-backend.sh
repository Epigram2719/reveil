#!/usr/bin/env sh

dotnet test \
  -c Debug \
  -r "$CI_PROJECT_DIR/cobertura" \
  --collect:"XPlat Code Coverage" \
  --test-adapter-path:. \
  --logger:"junit;LogFilePath=$CI_PROJECT_DIR/junit/junit-test-result.xml;MethodFormat=Class;FailureBodyFormat=Verbose"

total_coverage=0
count=0

for i in $(find "$CI_PROJECT_DIR/cobertura" -name '*.xml');
do
    printf "Found coverage report: %s\n" "$i"
    line_rate="$(head -n 2 ${i} | sed 'N;s/.*line-rate="\([^" ]*\).*/\1/g')"
    coverage=$(echo "${line_rate} * 100" | bc)
    printf "PARTIAL_COVERAGE: %2.2f\n" "$coverage"
    count=$((count + 1))
    total_coverage=$(echo "${total_coverage} + ${coverage}" | bc)
done;

printf "Found a total of %i report(s)\n" "$count"
printf "TOTAL_COVERAGE=%2.2f\n" "$(echo "${total_coverage} / ${count}" | bc -l)"
