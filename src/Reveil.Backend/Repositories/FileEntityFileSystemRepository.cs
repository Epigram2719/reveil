using System.IO;
using Microsoft.Extensions.Options;
using Reveil.Backend.Models.Configuration;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Repositories {
  public class FileEntityFileSystemRepository : AbstractGenericFileSystemPersistedRepository<FileEntity> {
    private readonly IOptions<PersistenceConfiguration> _persistenceConfiguration;

    public FileEntityFileSystemRepository(IOptions<PersistenceConfiguration> persistenceConfiguration)
      : base(persistenceConfiguration) {
      _persistenceConfiguration = persistenceConfiguration;
    }

    protected override string GetDataPath(FileEntity baseEntity) {
      var basePath = Path.Join(
        _persistenceConfiguration.Value.DataRoot,
        baseEntity.GetType().Name.ToLower(),
        baseEntity.CaseId,
        baseEntity.MessageId);
      Directory.CreateDirectory(basePath);
      return Path.Join(basePath, $"{baseEntity.Id}.json");
    }
  }
}
