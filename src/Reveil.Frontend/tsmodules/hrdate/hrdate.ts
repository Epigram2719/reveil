export class HrDate {
    private locale: string;

    constructor(locale: string) {
        this.locale = locale
    }

    GetTime(d: string): string {
        return new Date(d).toLocaleTimeString(this.locale)
    }

    GetDate(d: string): string {
        return new Date(d).toLocaleDateString(this.locale)
    }

    GetTimeDate(d: string): string {
        return new Date(d).toLocaleTimeString(this.locale) + " " + new Date(d).toLocaleDateString(this.locale)
    }
}
