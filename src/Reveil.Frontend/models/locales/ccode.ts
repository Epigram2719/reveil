export interface IModelLocalesCCode {
    name: string,
    code: string,
}
