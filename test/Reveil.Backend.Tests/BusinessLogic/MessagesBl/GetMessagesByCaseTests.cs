using System.Linq;
using System.Net;
using FluentAssertions;
using Microsoft.Extensions.Options;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Configuration;
using Reveil.Backend.Models.Entities;
using Reveil.Backend.Notifications;
using Reveil.Backend.Tests.PersistenceProvider;
using Xunit;

namespace Reveil.Backend.Tests.BusinessLogic.MessagesBl {
  public class GetMessagesByCaseTests {
    private readonly INotificationSenderFactory _devNullSenderFactory;
    public GetMessagesByCaseTests() {
      _devNullSenderFactory = new NotificationSenderFactory(Options.Create(new NotificationConfiguration()));
    }
    
    [Theory]
    [InlineData("0123456789ABCDEF", RepoType.InMemory)]
    [InlineData("0123456789ABCDEF", RepoType.FilePersisted)]
    public void TestNotExistingCaseReturns404ErrorResponse(string caseId, RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      var messages = RepositoryFactory.Create<MessageEntity>(repoType);
      var files = RepositoryFactory.Create<FileEntity>(repoType);
      var bl = new Backend.BusinessLogic.MessagesBl(cases, journals, messages, files, _devNullSenderFactory);
      var r = bl.GetMessagesByCase(caseId);
      r.IsFailure.Should().BeTrue();
      r.Error.Should().BeOfType<ErrorResponse>();
      r.Error.StatusCode.Should().Be(HttpStatusCode.NotFound);
      cases.Remove(_ => true);
      journals.Remove(_ => true);
      messages.Remove(_ => true);
      files.Remove(_ => true);
    }

    [Theory]
    [InlineData("0123456789ABCDEF", RepoType.InMemory)]
    [InlineData("0123456789ABCDEF", RepoType.FilePersisted)]
    public void TestNotExistingMessageForExistingCaseReturns200Response(string caseId, RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      var messages = RepositoryFactory.Create<MessageEntity>(repoType);
      var files = RepositoryFactory.Create<FileEntity>(repoType);
      var bl = new Backend.BusinessLogic.MessagesBl(cases, journals, messages, files, _devNullSenderFactory);
      cases.Add(new CaseEntity {Id = caseId});
      var r = bl.GetMessagesByCase(caseId);
      r.IsSuccess.Should().BeTrue();
      r.Value.Should().BeOfType<ChildrenListResponse>();
      r.Value.StatusCode.Should().Be(HttpStatusCode.OK);
      r.Value.Ids.Should().HaveCount(0);
      cases.Remove(_ => true);
      journals.Remove(_ => true);
      messages.Remove(_ => true);
      files.Remove(_ => true);
    }

    [Theory]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF", RepoType.InMemory)]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF", RepoType.FilePersisted)]
    public void TestGetSingleMessageForExistingCaseReturns200Response(string caseId, string messageId, RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      var messages = RepositoryFactory.Create<MessageEntity>(repoType);
      var files = RepositoryFactory.Create<FileEntity>(repoType);
      var bl = new Backend.BusinessLogic.MessagesBl(cases, journals, messages, files, _devNullSenderFactory);

      cases.Add(new CaseEntity {Id = caseId});
      messages.Add(new MessageEntity {Id = messageId, CaseId = caseId});
      messages.Add(new MessageEntity {Id = "SOME MESSAGE ID", CaseId = "NOT THE QUERIED CASE ID"});

      var r = bl.GetMessagesByCase(caseId);
      r.IsSuccess.Should().BeTrue();
      r.Value.Should().BeOfType<ChildrenListResponse>();
      r.Value.StatusCode.Should().Be(HttpStatusCode.OK);
      r.Value.Ids.Should().HaveCount(1).And.Contain(messageId);
      cases.Remove(_ => true);
      journals.Remove(_ => true);
      messages.Remove(_ => true);
      files.Remove(_ => true);
    }

    [Theory]
    [InlineData("123", 0, RepoType.InMemory)]
    [InlineData("456", 1, RepoType.InMemory)]
    [InlineData("123", 50, RepoType.InMemory)]
    [InlineData("456", 100, RepoType.InMemory)]
    [InlineData("123", 0, RepoType.FilePersisted)]
    [InlineData("456", 1, RepoType.FilePersisted)]
    [InlineData("123", 50, RepoType.FilePersisted)]
    [InlineData("456", 100, RepoType.FilePersisted)]
    public void TestGetMultipleMessagesForExistingCaseReturns200Response(string caseId, int count, RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      var messages = RepositoryFactory.Create<MessageEntity>(repoType);
      var files = RepositoryFactory.Create<FileEntity>(repoType);
      var bl = new Backend.BusinessLogic.MessagesBl(cases, journals, messages, files, _devNullSenderFactory);
      cases.Add(new CaseEntity {Id = caseId});

      Enumerable.Range(0, count)
        .Select(c => new MessageEntity {CaseId = caseId, Id = c.ToString()})
        .ToList()
        .ForEach(m => messages.Add(m));

      var r = bl.GetMessagesByCase(caseId);
      r.IsSuccess.Should().BeTrue();
      r.Value.Should().BeOfType<ChildrenListResponse>();
      r.Value.StatusCode.Should().Be(HttpStatusCode.OK);
      r.Value.Ids.Should().HaveCount(count);
      cases.Remove(_ => true);
      journals.Remove(_ => true);
      messages.Remove(_ => true);
      files.Remove(_ => true);
    }
  }
}
