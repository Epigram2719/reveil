import qrcode from 'qrcode'

export class QrCode {
    constructor() {
        
    }

    async GenerateSVG(data: string): Promise<string> {
        return await qrcode.toString(data, { type: 'svg'})

    }

    async GenerateText(data: string): Promise<string> {
        return await qrcode.toString(data, { type: 'utf8'})
    }
}
