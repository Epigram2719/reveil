using FluentAssertions;
using Reveil.Backend.Models.Configuration.Notification;
using Xunit;

namespace Reveil.Backend.Tests.Configuration {
  public class NotificationConfigurationLogicTests {
    [Theory]
    [InlineData("Discord")]
    [InlineData("DISCORD")]
    [InlineData("discord")]
    [InlineData("webhook")]
    [InlineData("WebHook")]
    [InlineData("WEBHOOK")]
    [InlineData("Webhook")]
    [InlineData("Smtp")]
    [InlineData("smtp")]
    [InlineData("SMTP")]
    public void TestCanUserGivenTypeMapToConcreteConfigurationType(string userGivenType) {
      var availableConfigurations = NotificationConfigurationLogic.GetAvailableNotificationTargetConfigurationTypes();
      availableConfigurations.GetNotificationTargetConfigurationTypeByName(userGivenType).Should().NotBeNull();
    }

    [Theory]
    [InlineData("")]
    [InlineData(" ")]
    [InlineData(null)]
    [InlineData("AAAA-AAAA-AAAA-AAAA-AAAA-AAAA-AAAA")]
    [InlineData("Not:Existing_Auth-Provider*With?Special!Character;In$The%Name&/~$%&/()=?!c")]
    public void TestNotKnownUserGivenTypeDoesNotReturnAValidConfigurationType(string userGivenType) {
      var availableConfigurations = NotificationConfigurationLogic.GetAvailableNotificationTargetConfigurationTypes();
      availableConfigurations.GetNotificationTargetConfigurationTypeByName(userGivenType).Should().BeNull();
    }
  }
}
