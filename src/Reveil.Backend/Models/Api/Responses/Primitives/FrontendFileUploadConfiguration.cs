// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using System.Collections.Generic;

namespace Reveil.Backend.Models.Api.Responses.Primitives {
  public class FrontendFileUploadConfiguration {
    public List<MediaType> SupportedMediaTypes { get; set; }
    public int MaxFileSizeInBytes { get; set; }
  }
}
