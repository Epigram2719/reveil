import { ApiCryptModsIMod } from './mods/imod'
import { ApiCryptModsPGP10 } from './mods/pgp10'
import { ApiCryptModsPlain } from './mods/plain'

import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'

export enum ApiCryptType {
    Plain = 1,
    PGP10,
}

export class ApiCrypt {
    static Proxy<T>(crypt_type: ApiCryptType, inject: ApiEndpointExtensionLayer): ApiCryptModsIMod<T> {
        if(crypt_type === ApiCryptType.Plain) {
            return new ApiCryptModsPlain<T>(inject);
        }
        if(crypt_type === ApiCryptType.PGP10) {
            return new ApiCryptModsPGP10<T>(inject);
        }

        throw Error("Error. Module not found.")
    }

    static ApiCryptTypeByIndex(index: Number): ApiCryptType {
        if(index === 1) {
            return ApiCryptType.Plain
        }
        if(index === 2) {
            return ApiCryptType.PGP10
        }
        throw Error("Unable to find ApiCryptType with index " + index)
    }
}
