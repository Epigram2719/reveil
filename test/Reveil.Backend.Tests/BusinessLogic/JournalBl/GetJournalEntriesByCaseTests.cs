using System.Linq;
using System.Net;
using FluentAssertions;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Entities;
using Reveil.Backend.Repositories;
using Xunit;

namespace Reveil.Backend.Tests.BusinessLogic.JournalBl {
  public class GetJournalEntriesByCaseTests {
    [Theory]
    [InlineData("")]
    [InlineData(" ")]
    [InlineData(null)]
    [InlineData("0123456789ABCDEF")]
    public void TestNotExistingCaseReturns404ErrorResponse(string caseId) {
      var bl = new Backend.BusinessLogic.JournalBl(new GenericInMemoryRepository<CaseEntity>(), new GenericInMemoryRepository<JournalEntity>());

      var r = bl.GetJournalEntriesByCase(caseId);
      r.IsFailure.Should().BeTrue();
      r.Error.Should().BeOfType<ErrorResponse>();
      r.Error.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }

    [Theory]
    [InlineData("")]
    [InlineData(" ")]
    [InlineData(null)]
    [InlineData("0123456789ABCDEF")]
    public void TestNotExistingJournalForExistingCaseReturns200Response(string caseId) {
      var cases = new GenericInMemoryRepository<CaseEntity>();
      cases.Add(new CaseEntity {Id = caseId});

      var bl = new Backend.BusinessLogic.JournalBl(cases, new GenericInMemoryRepository<JournalEntity>());
      var r = bl.GetJournalEntriesByCase(caseId);
      r.IsSuccess.Should().BeTrue();
      r.Value.Should().BeOfType<ChildrenListResponse>();
      r.Value.StatusCode.Should().Be(HttpStatusCode.OK);
      r.Value.Ids.Should().HaveCount(0);
    }

    [Theory]
    [InlineData("", "")]
    [InlineData(" ", " ")]
    [InlineData(null, null)]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF")]
    public void TestGetSingleJournalEntryForExistingCaseReturns200Response(string caseId, string messageId) {
      var cases = new GenericInMemoryRepository<CaseEntity>();
      cases.Add(new CaseEntity {Id = caseId});
      var journalEntries = new GenericInMemoryRepository<JournalEntity>();
      journalEntries.Add(new JournalEntity {Id = messageId, CaseId = caseId});

      var bl = new Backend.BusinessLogic.JournalBl(cases, journalEntries);
      var r = bl.GetJournalEntriesByCase(caseId);
      r.IsSuccess.Should().BeTrue();
      r.Value.Should().BeOfType<ChildrenListResponse>();
      r.Value.StatusCode.Should().Be(HttpStatusCode.OK);
      r.Value.Ids.Should().HaveCount(1).And.Contain(messageId);
    }

    [Theory]
    [InlineData("123", 0)]
    [InlineData("456", 1)]
    [InlineData("123", 50)]
    [InlineData("456", 100)]
    public void TestGetMultipleJournalEntriesForExistingCaseReturns200Response(string caseId, int count) {
      var cases = new GenericInMemoryRepository<CaseEntity>();
      cases.Add(new CaseEntity {Id = caseId});

      var journalEntries = new GenericInMemoryRepository<JournalEntity>();
      Enumerable.Range(0, count)
        .Select(c => new JournalEntity {CaseId = caseId, Id = c.ToString()})
        .ToList()
        .ForEach(m => journalEntries.Add(m));

      var bl = new Backend.BusinessLogic.JournalBl(cases, journalEntries);

      var r = bl.GetJournalEntriesByCase(caseId);
      r.IsSuccess.Should().BeTrue();
      r.Value.Should().BeOfType<ChildrenListResponse>();
      r.Value.StatusCode.Should().Be(HttpStatusCode.OK);
      r.Value.Ids.Should().HaveCount(count);
    }
  }
}
