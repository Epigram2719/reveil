// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using System;
using System.Net;

namespace Reveil.Backend.Models.Api.Responses.Core {
  public class SuccessResponse : BaseResponse {
    public string Message { get; set; }

    public static SuccessResponse Create(HttpStatusCode statusCode, string message = null) {
      return new SuccessResponse {
        IsSuccess = true,
        Message = message ?? statusCode.ToString(),
        StatusCode = statusCode,
        DateTime = DateTime.UtcNow
      };
    }
  }
}
