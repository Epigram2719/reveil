import * as api from '~/api'

export interface IModelFile {
    response: api.FileMetadataResponse
}
