using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Reveil.Backend {
  public static class Program {
    public static Task Main(string[] args) => Host
      .CreateDefaultBuilder(args)
      .ConfigureAppConfiguration(ConfigureAppConfiguration)
      .ConfigureWebHostDefaults(webBuilder => webBuilder
        .ConfigureKestrel(k => k
          .ConfigureEndpointDefaults(d => d.Protocols = HttpProtocols.Http1AndHttp2))
        .UseStartup<Startup>())
      .Build()
      .RunAsync();

    private static void ConfigureAppConfiguration(HostBuilderContext context, IConfigurationBuilder builder) {
      builder
        .AddJsonFile("appsettings.json", false, true)
        .AddJsonFile("appsettings.Development.json", true, true)
        .AddJsonFile("Data/appsettings.json", true, true)
        .AddUserSecrets<Startup>(true)
        .AddEnvironmentVariables();
    }
  }
}
