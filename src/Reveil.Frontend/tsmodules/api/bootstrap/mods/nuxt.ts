import * as api from '~/api'

import { IModelBootstrap } from '../models/bootstrap'
import { val_global_default, val_global_override } from '../values'
import { IModelAuthMgmt, IModelAuthUser } from '~/tsmodules/api/auth/models'

import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'
import { ApiCache, ApiCacheModsIMod, ApiCacheType} from '~/tsmodules/api/cache'
import { ApiCrypt, ApiCryptModsIMod, ApiCryptType } from '~/tsmodules/api/crypt'
import { ApiBootstrap, ApiBootstrapModsIMod, ApiBootstrapType } from '~/tsmodules/api/bootstrap'

export class ApiBootstrapModsNuxt implements ApiBootstrapModsIMod {
    private inject: ApiEndpointExtensionLayer

    private cache: ApiCacheModsIMod<api.BootstrapConfigurationResponse>

    constructor(inject: ApiEndpointExtensionLayer) {
        this.inject = inject

        this.cache = ApiCache.Proxy<api.BootstrapConfigurationResponse>(this.inject.CacheType, "bootstrap", this.inject)
    }

    async GetValues(val_local_default: null | object = null, val_local_override: null | object = null): Promise<IModelBootstrap> {
        // values
        var val_default = (val_local_default === null) ? val_global_default : val_local_default
        var val_override = (val_local_override === null) ? val_global_override : val_local_override

        // cache
        if(this.cache.GetCache() !== null) {
            return Promise.resolve(Object.assign(val_default, this.cache.GetCache()!, val_override))
        }
        
        // api
        else {
            return this.inject.Context!.$axios.get<api.BootstrapConfigurationResponse>('/bootstrap/config.json')
            .then((e) => {
                this.cache.SetCache(e.data)
                return Object.assign(val_default, e.data, val_override)
            })
            .catch((e) => {
                throw Error('ApiBootstrapModsNuxt => GetValues')
            })
        } 
    }

    async GetApiCfg(): Promise<api.Configuration> {
        // mam - ModelAuthUser
        var mam = ApiCache.Proxy<IModelAuthMgmt>(this.inject.CacheType, 'authmgmt', this.inject).GetCache()
        var mam_token = ""
        if(mam !== null) {
            mam_token = mam.response.authorizationToken!
        }

        // mau - ModelAuthUser
        var mau = ApiCache.Proxy<IModelAuthUser>(this.inject.CacheType, 'authuser', this.inject).GetCache();
        var mau_token = ""
        if(mau !== null) {
            mau_token = mau.computed_token
        }

        var v = await this.GetValues()
        var a = location.protocol + "//" + v.api;

        var rtvl = new api.Configuration({
            apiKey: '',
            username: '',
            password: '',
            accessToken: '',
            basePath: a || '',
            baseOptions: {
                headers: {
                    'Bearer': mam_token || mau_token || '',
                },
            },
        })
        return rtvl

    }
}
