using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Entities;
using Reveil.Backend.Repositories;

namespace Reveil.Backend.IntegrationTests {
  public class ReveilTestWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class {
    protected override void ConfigureWebHost(IWebHostBuilder builder) {
      var authorizationEntries = new GenericInMemoryRepository<AuthorizationEntity>();
      authorizationEntries.Add(new AuthorizationEntity {
        Id = Guid.NewGuid().ToString(),
        AuthorizationToken = WellKnownConstants.IntegrationTestAuthorizationToken,
        UserId = WellKnownConstants.IntegrationTestUserId,
        ValidTo = DateTime.UtcNow.AddDays(1)
      });
      
      builder.ConfigureServices(services => {
        services.AddSingleton<IGenericRepository<UserEntity>, GenericInMemoryRepository<UserEntity>>();
        services.AddSingleton<IGenericRepository<CaseEntity>, GenericInMemoryRepository<CaseEntity>>();
        services.AddSingleton<IGenericRepository<FileEntity>, GenericInMemoryRepository<FileEntity>>();
        services.AddSingleton<IGenericRepository<MessageEntity>, GenericInMemoryRepository<MessageEntity>>();
        services.AddSingleton<IGenericRepository<JournalEntity>, GenericInMemoryRepository<JournalEntity>>();
        services.AddSingleton<IGenericRepository<ContentEntity>, GenericInMemoryRepository<ContentEntity>>();
        services.AddSingleton<IGenericRepository<AuthorizationEntity>>(authorizationEntries);
      });
      builder.ConfigureAppConfiguration(configuration => {
      });
    }

    protected override IHostBuilder CreateHostBuilder() => Host
      .CreateDefaultBuilder()
      .ConfigureWebHostDefaults(webHostBuilder => webHostBuilder.UseStartup<Startup>().UseTestServer());
  }
}
